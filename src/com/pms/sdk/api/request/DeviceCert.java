package com.pms.sdk.api.request;

import android.annotation.SuppressLint;
import android.content.Context;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.messaging.FirebaseMessaging;
import com.pms.sdk.api.APIManager.APICallback;
import com.pms.sdk.common.util.CLog;
import com.pms.sdk.common.util.DateUtil;
import com.pms.sdk.common.util.PMSUtil;
import com.pms.sdk.common.util.PhoneState;
import com.pms.sdk.common.util.Prefs;
import com.pms.sdk.common.util.StringUtil;
import com.pms.sdk.view.Badge;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URI;

public class DeviceCert extends BaseRequest {

	public DeviceCert(Context context) {
		super(context);
	}

	/**
	 * get param
	 * 
	 * @return
	 */
	public JSONObject getParam (JSONObject userData) {
		JSONObject jobj;

		try {
			jobj = new JSONObject();

			// new version
			jobj.put("appKey", PMSUtil.getApplicationKey(mContext));
			jobj.put("uuid", PMSUtil.getUUID(mContext));
			jobj.put("pushToken", PMSUtil.getGCMToken(mContext));
			jobj.put("custId", PMSUtil.getCustId(mContext));
			jobj.put("memberId", PMSUtil.getMemberId(mContext));
			jobj.put("appVer", PhoneState.getAppVersion(mContext));
			jobj.put("os", "A");
			jobj.put("osVer", PhoneState.getOsVersion());
			jobj.put("device", PhoneState.getDeviceName());
			jobj.put("sessCnt", "1");

			if (userData != null) {
				jobj.put("userData", userData);
			}

			return jobj;
		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * request
	 * 
	 * @param apiCallback
	 */
	public void request (final JSONObject userData, final APICallback apiCallback) {

		String custId = PMSUtil.getCustId(mContext);

		boolean isNExistsUUID = PhoneState.createDeviceToken(mContext);
		CLog.i("Device Cert request get UUID : " + (isNExistsUUID == true ? "Created" : "Exists"));
		// gcm 사용안하기에 없앰
		// 앱이 처음 수행 됐을 때, push token을 받아오기 전 까지 device cert를 수행하지 않게함
					/*int checkCnt = 0;
					while (checkCnt < 15 && (StringUtil.isEmpty(PMSUtil.getGCMToken(mContext)) || NO_TOKEN.equals(PMSUtil.getGCMToken(mContext)))) {
						try {
							CLog.i("deviceCert:pushToken is null, sleep(2000)");
							checkCnt++;
							Thread.sleep(2000);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}*/
		if (!custId.equals(mPrefs.getString(PREF_LOGINED_CUST_ID))) {
			// 기존의 custId와 auth할려는 custId가 다르다면, DB초기화
			CLog.i("DeviceCert:new user");
			mDB.deleteAll();
		}
		FirebaseMessaging.getInstance().getToken()
				.addOnCompleteListener(new OnCompleteListener<String>() {
					@Override
					public void onComplete(@NonNull Task<String> task) {
						if(task.isSuccessful()){
							String token = task.getResult();
							PMSUtil.setGCMToken(mContext, token);
						}
						if (StringUtil.isEmpty(PMSUtil.getGCMToken(mContext)) || PMSUtil.getGCMToken(mContext).equals(NO_TOKEN)) {
							// check push token
							CLog.i("DeviceCert:no push token");
							mPrefs.putString(KEY_GCM_TOKEN, NO_TOKEN);
						}
						try
						{
							apiManager.call(API_DEVICE_CERT, getParam(userData), new APICallback() {
								@Override
								public void response (String code, JSONObject json) {
									if (CODE_SUCCESS.equals(code)) {
										PMSUtil.setDeviceCertStatus(mContext, DEVICECERT_COMPLETE);
										requiredResultProc(json);
									}

									apiCallback.response(code, json);
								}
							});
						}
						catch (Exception e)
						{
							CLog.e(e.toString());
						}
					}
				});

		CLog.i("DeviceCert:validate ok");
	}

	/**
	 * required result proccess
	 * 
	 * @param json
	 */
	@SuppressLint("DefaultLocale")
	private boolean requiredResultProc (JSONObject json) {
		try {
			PMSUtil.setAppUserId(mContext, json.getString("appUserId"));
			PMSUtil.setEncKey(mContext, json.getString("encKey"));

			// set msg flag
			mPrefs.putString(PREF_API_LOG_FLAG, json.getString("collectApiLogFlag"));
			mPrefs.putString(PREF_PRIVATE_LOG_FLAG, json.getString("collectPrivateLogFlag"));
			mPrefs.putString(PREF_DUMP_FLAG, json.getString("dupFlag"));
			String custId = PMSUtil.getCustId(mContext);
			if (!StringUtil.isEmpty(custId)) {
				mPrefs.putString(PREF_LOGINED_CUST_ID, PMSUtil.getCustId(mContext));
			}

			// set badge
			Badge.getInstance(mContext).updateBadge(json.getString("newMsgCnt"));

			// set config flag
			if ((mPrefs.getString(PREF_MSG_FLAG).equals(json.getString("msgFlag")) == false)
					|| (mPrefs.getString(PREF_NOTI_FLAG).equals(json.getString("notiFlag")) == false)) {
				new SetConfig(mContext).request(mPrefs.getString(PREF_MSG_FLAG), mPrefs.getString(PREF_NOTI_FLAG), null);
			}

			// readMsg
			final JSONArray readArray = PMSUtil.arrayFromPrefs(mContext, PREF_READ_LIST);
			if (readArray.length() > 0) {
				// call readMsg
				new ReadMsg(mContext).request(readArray, new APICallback() {
					@Override
					public void response (String code, JSONObject json) {
						if (CODE_SUCCESS.equals(code)) {
							// delete readMsg
							new Prefs(mContext).putString(PREF_READ_LIST, "");
						}

						if (CODE_PARSING_JSON_ERROR.equals(code)) {
							for (int i = 0; i < readArray.length(); i++) {
								try {
									if ((readArray.get(i) instanceof JSONObject) == false) {
										readArray.put(i, PMSUtil.getReadParam(readArray.getString(i)));
									}
								} catch (JSONException e) {
									e.printStackTrace();
								}
							}
							new ReadMsg(mContext).request(readArray, new APICallback() {
								@Override
								public void response (String code, JSONObject json) {
									if (CODE_SUCCESS.equals(code)) {
										new Prefs(mContext).putString(PREF_READ_LIST, "");
									}
								}
							});
						}
					}
				});
			} else {
				CLog.i("readArray is null");
			}

			// clickMsg
			JSONArray clickArray = PMSUtil.arrayFromPrefs(mContext, PREF_CLICK_LIST);
			if (clickArray.length() > 0) {
				// call clickMsg
				new ClickMsg(mContext).request(clickArray, new APICallback() {
					@Override
					public void response (String code, JSONObject json) {
						if (CODE_SUCCESS.equals(code)) {
							// delete clickMsg
							new Prefs(mContext).putString(PREF_CLICK_LIST, "");
						}
					}
				});
			} else {
				CLog.i("clickArray is null");
			}

			// mqttFlag Y/N
			if (FLAG_Y.equals(PMSUtil.getMQTTFlag(mContext))) {
				String flag = json.getString("privateFlag");
				mPrefs.putString(PREF_PRIVATE_FLAG, flag);
				if (FLAG_Y.equals(flag)) {
					String protocol = json.getString("privateProtocol");
					String protocolTemp = "";
					try {
						URI uri = new URI(PMSUtil.getMQTTServerUrl(mContext, 1));
						if (protocol.equals(PROTOCOL_TCP)) {
							protocolTemp = protocol.toLowerCase() + "cp";
						} else if (protocol.equals(PROTOCOL_SSL)) {
							protocolTemp = protocol.toLowerCase() + "sl";
						}

						if (uri.getScheme().equals(protocolTemp)) {
							mPrefs.putString(PREF_PRIVATE_PROTOCOL, protocol);
						} else {
							mPrefs.putString(PREF_PRIVATE_PROTOCOL, protocol);
//							mContext.stopService(new Intent(mContext, MQTTService.class));
						}
					} catch (NullPointerException e) {
						mPrefs.putString(PREF_PRIVATE_PROTOCOL, protocol);
					}
//					if (device < Build.VERSION_CODES.M && device < 23)
//					{
//						Intent i = new Intent(mContext, RestartReceiver.class);
//						i.setAction(ACTION_FORCE_START);
//						mContext.sendBroadcast(i);
//					}
				} else {
//					mContext.stopService(new Intent(mContext, MQTTService.class));
				}
			}

			// LogFlag Y/N
			if ((FLAG_N.equals(mPrefs.getString(PREF_API_LOG_FLAG)) && FLAG_N.equals(mPrefs.getString(PREF_PRIVATE_LOG_FLAG))) == false) {
				setCollectLog();
			}

			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	private void setCollectLog () {
		boolean isAfter = false;
		String beforeDate = mPrefs.getString(PREF_YESTERDAY);
		String today = DateUtil.getNowDateMo();

		try {
			isAfter = DateUtil.isDateAfter(beforeDate, today);
		} catch (Exception e) {
			isAfter = false;
		}

		if (isAfter) {
			mPrefs.putBoolean(PREF_ONEDAY_LOG, false);
		}

		if (mPrefs.getBoolean(PREF_ONEDAY_LOG) == false) {
			if (beforeDate.equals("")) {
				beforeDate = DateUtil.getNowDateMo();
				mPrefs.putString(PREF_YESTERDAY, beforeDate);
			}
			new CollectLog(mContext).request(beforeDate, null);
		}
	}
}
