package com.pms.sdk;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;

import com.pms.sdk.bean.Msg;
import com.pms.sdk.bean.MsgGrp;
import com.pms.sdk.common.util.CLog;
import com.pms.sdk.common.util.PMSUtil;
import com.pms.sdk.common.util.Prefs;
import com.pms.sdk.common.util.StringUtil;
import com.pms.sdk.db.PMSDB;
import com.pms.sdk.view.Badge;

import java.io.Serializable;
import java.util.Stack;

/**
 * @since 2012.12.28
 * @author erzisk
 * @description pms (solution main class)
 * @version [2013.10.07] pushMsg.java 생성 - push 데이터 key 변경<br>
 *          [2013.10.11] android-query -> volley 로 library 변경<br>
 *          [2013.10.14] push notification 출력 변경<br>
 *          [2013.10.16 15:09] logout시에 저장된 cust_id삭제<br>
 *          [2013.10.21 10:30] api call시에, appUserId 또는 encKey가 없을 시 에러 추가<br>
 *          [2013.10.21 11:22] api call시에, paging이 존재하는 api(newMsg.m)일 경우, 가장 마지막 호출에서만 apiCallback이 이뤄지게끔 수정<br>
 *          [2013.10.21 18:43] push 데이터 key 변경 (IPMSConsts)<br>
 *          [2013.10.25 14:15] deviceCert시 token이 없는경우 'noToken'으로 넘김<br>
 *          [2013.10.29 17:03] deviceCert시 token이 "noToken"일 경우도 loop<br>
 *          [2013.10.30 11:26] push 수신 시 notImg exception 처리 추가<br>
 *          [2013.11.01 11:15] CLog 새로운 버전으로 적용 및 소스에서 TAG 삭제!!!!<br>
 *          [2013.11.06 15:02] deviceCert parameter에 sessCnt추가<br>
 *          [2013.11.06 16:07] logined_cust_id 추가, deviceCert 및 loginPms시에 cust_id와 logined_cust_id를 비교하여 db delete 결정<br>
 *          [2013.11.15 10:38] APIManager에서 error발생 시에도 apiCalback수행<br>
 *          [2013.11.19 15:36] push popup finish thread 추가<br>
 *          [2013.11.26 09:22] click 관련 추가<br>
 *          [2013.11.26 16:42] MQTT Client 변경<br>
 *          [2013.11.26 21:13] deviceCert시 read 및 click api 호출<br>
 *          [2013.11.27 17:51] MQTT Client 사용시 로그 저장 루틴 추가 <br>
 *          [2013.12.04 17:47] push 수신 시 해당 msgId가 존재하면 noti하지 않게 변경<br>
 *          [2013.12.05 15:35] push popup 유지 시간 설정 추가 PMS.setPushPopupShowingTime(int)<br>
 *          [2013.12.10 18:21] MQTT KeepAlive Time mata-data에서 설정하도록 수정함.<br>
 *          [2013.12.11 16:07] PMSDB.deleteExpireMsg 로직 변경<br>
 *          [2013.12.12 18:34] Private Flag 추가<br>
 *          [2013.12.16 10:33] PushReceiver.onMessage에 synchronized추가<br>
 *          [2013.12.16 16:36] Push Popup 사용자가 설정할수 있도록 변경함. <br>
 *          [2013.12.17 13:19] MQTT Client 중복 실행 수정함. <br>
 *          [2014.01.13 09:31] MQTT Client 안정화 & Connect Check 부분 수정함. <br>
 *          [2014.02.07 17:12] Collect 부분 수정함. <br>
 *          [2014.02.11 10:18] CollectLog getParam 부분 수정함. <br>
 *          [2014.02.21 17:23] MQTT Client 안정화 및 Prvate 서버 Protocol 적용 가능하게 수정함.<br>
 *          [2014.02.25 10:12] 안쓰는 method 삭제함.<br>
 *          [2014.03.03 10:36] 팝업 셋팅 default 설정 가능하게 변경.<br>
 *          [2014.03.05 15:31] MQTT Client 관련 Service 추가.<br>
 *          [2014.03.05 20:37] MQTT Client STOP 재시작하는 현상 수정.<br>
 *          [2014.03.13 10:47] MQTT Client keepAlive 수정함. sleep mode 삭제.<br>
 *          [2014.03.28 09:29] DisusePms.m API 추가함.<br>
 *          [2014.05.14 20:06] DisusePms.m API 삭제 & Popup 설정값 추가.<br>
 *          [2014.06.02 20:39] Popup WebView background 수정함. <br>
 *          [2014.06.05 18:24] 다른앱 실행시 팝업창 미 노출. <br>
 *          [2014.06.09 11:05] 팝업창에 대한 Flag 값 추가함. <br>
 *          [2014.06.11 17:32] 텍스트 푸쉬일때 toast 메세지로 전환할수 있도록 수정. <br>
 *          [2014.06.16 20:44] 다른앱 사용시 안뜨는 플래그 추가함. <br>
 *          [2014.06.24 09:10] ReadMSg UserMsgId로 가능하게 수정함. <br>
 *          [2014.07.02 15:28] 연속 발송시 1건씩 빠지는 문제 수정함. <br>
 *          [2014.07.03 12:28] MQTT Wake Lock 쪽 예외처리 추가함. <br>
 *          [2014.07.15 15:20] MQTT ping 관련 소스 추가함. 필요없는 소스 삭제. <br>
 *          [2014.07.24 10:41] MQTT 버그 수정 & pushImg 저장 가능하게 수정함.<br>
 *          [2014.07.25 10:30] 자동 ReadMsg 104 오류발생시 JSON 재구성해서 다시 호출하게 수정함.<br>
 *          [2014.07.25 10:30] GCM Code제서 & ChangeDevice.m 추가함.<br>
 *          [2014.09.29 16:57] Vollery No Cache 로 변경함. <br>
 *          [2014.10.06 13:53] Notification bar title & context token 값 수정함. <br>
 *          [2014.10.21 14:29] Android OS 5.0에서 상태창 이미지 표시 안되는 버그 수정함. <br>
 *          [2014.11.17 13:28] MQTT Client URL 자동적으로 선택 가능하게 수정함. <br>
 *          [2014.11.26 20:42] PushPopup시 Sleep을 넣어서 확인 할수 있게 변경. <br>
 *          [2014.12.01 17:37] 팝업 Activity 실행시 Receiver 단으로 전송함. <br>
 *          [2014.12.02 10:05] Sleep 부분삭제, Receiver 부분삭제, URL 자동선택 부분 버그 수정함. <br>
 *          [2014.12.02 16:35] URL 자동선택 부분 버그 수정함. <br>
 *          [2014.12.02 21:38] 최초 접속시 3번 연속 연결후 다른 서버로 변경함. <br>
 *          [2014.12.03 10:51] 디바이스 네트워크 False시 서비스 멉추게 수정함. <br>
 *          [2014.12.15 09:15] GCM code 삽입 서비스 체크하는 루틴 삽입함. <br>
 *          [2015.01.30 14:31] NOTI FLAG 삭제! <br>
 *          [2015.03.13 16:54] 구글 정책에 따른 앱 실행중일떄만 팝업창 노출. <br>
 *          [2015.03.30 13:20] MQTT Connect시 Exception 처리함. <br>
 *          [2015.04.08 14:35] Sender Ping 호출시 오류 부분 Exception처리함. <br>
 *          [2015.06.10 16:49] MQTT Lib 업데이트함. <br>
 *          [2015.06.30 11:37] 재접속시 2초 간격을 둠. <br>
 *          [2015.07.15 13:19] MQTT Lib 업데이트함. <br>
 *          [2016.04.29 10:08] X509TrustManager 코드 삭제하고 대응 코드 삽입함. <br>
 *          [2016.11.29 09:45] doze mode broadcast 패치1 적용 <br>
 *          [2016.12.14 13:21] Icon에 대한 버그 사항 수정함.<br>
 *          [2017.03.29 15:52] GCM 사용안함에 따라 token값 가져오기위한 딜레이 주석<br>
 *          [2017.10.25 17:25] - Queue Manager 추가 Single tone 으로 관리 (Image Loader 및 APIManager)
 *          					- DB 중복 Open으로 인해 강제종료 문제 수정
 *          					- PushPopup 발생 시 강제 종료 문제 수정
 *          					- UUID 생성 로직 변경, Device정보를 이용하여 UUID를 작성하는 경로 이전에 Device 정보 없이 발급하는 방법으로
 *          					- UUID 발급, 발급 실패 시 Device정보를 필요로 하는 UUID 발급 방법으로 발급 시도함 <br>
 *			[2018.06.14 10:41] Android 8.0 대응
 *			[2018.07.11 10:03] Android NotificationChannel 대응, JobScheduler 대응
 *			[2018.07.25 13:19] 한화 요청으로 compileSdkVersion 23으로 수정
 *			[2018.07.26 14:36] 암시적 Intent 대응 수정
 *			[2018.08.20 14:01] 암시적 Intent 대응 수정, Notification 소리 부분 수정, MQTT 라이브러리 교체, WRITE_EXTERNAL_STORAGE 제거
 *			[2018.08.21 09:42] MQTT appuserid 방어코드 강화
 *			[2018.08.28 10:17] compileSdkVersion26 대응, MQTT 에러 대응, NotificationChannelGroup대응
 *			[2018.08.30 11:26] 업체요청으로 notification Id 고정, 벳지 미표시
 *			[2018.09.03 15:20] MQTT 간소화, Android 8.0 대응, PMSUtil 오류대응, Notification 벨소리 조절 대응
 *			[2018.09.12 16:35] MQTT 간소화 오류 대응, 벨소리 대응 추가
 *			[2018.10.17 15:31] Android 8.0 대응, 벨소리 채널 대응, MQTT 대응
 *			[2018.10.25 12:57] 8.0 아래에서 MQTT 백그라운드 무제한
 *		    [2018.10.25 18:29] MQTT 수정
 *		    [2018.10.26 09:10] MQTT 수정
 *		    [2018.12.19 11:05] MQTTBinder 적용, 벨소리 재생 개선
 *		    [2019.01.08 14:15] MQTTStarter 삭제, 분기별 서비스 실행 적용
 *		    [2019.01.16 17:25] MQTTBinder keepAliveTime 적용
 *		    [2019.02.12 11:23] FCM 대응, FCMRequestToken 추가
 *		    [2019.04.05 14:39] 벨소리 볼륨 로직 제거
 *		    [2019.08.26 15:17] MQTTBinder 적용, ReadMsg kk->HH 적용, ServiceCheckReceiver,MQTTService 삭제
 *		    [2020.10.20 16:09] AndroidX 대응
 */
public class PMS implements IPMSConsts, Serializable {

	private static final long serialVersionUID = 1L;

	public static final int FLAG_EXCLUDE_STOPPED_PACKAGES = 16;

	private static PMS instancePms;
	private static PMSPopup instancePmsPopup;
	private Context mContext;

	private final PMSDB mDB;
	private final Prefs mPrefs;

	private OnReceivePushListener onReceivePushListener;

	/**
	 * constructor
	 * 
	 * @param context
	 */
	private PMS(Context context) {
		this.mDB = PMSDB.getInstance(context);
		this.mPrefs = new Prefs(context);
		CLog.i("Version:" + PMS_VERSION + ",UpdateDate:"+PMS_VERSION_UPDATE_DATE);

		initOption(context);
	}

	/**
	 * initialize option
	 */
	private void initOption (Context context) {
		if (StringUtil.isEmpty(mPrefs.getString(PREF_NOTI_FLAG))) {
			mPrefs.putString(PREF_NOTI_FLAG, "Y");
		}
		if (StringUtil.isEmpty(mPrefs.getString(PREF_MSG_FLAG))) {
			mPrefs.putString(PREF_MSG_FLAG, "Y");
		}
		if (StringUtil.isEmpty(mPrefs.getString(PREF_RING_FLAG))) {
			mPrefs.putString(PREF_RING_FLAG, "Y");
		}
		if (StringUtil.isEmpty(mPrefs.getString(PREF_VIBE_FLAG))) {
			mPrefs.putString(PREF_VIBE_FLAG, "Y");
		}
		if (StringUtil.isEmpty(mPrefs.getString(PREF_ALERT_FLAG))) {
			mPrefs.putString(PREF_ALERT_FLAG, "Y");
		}
		if (StringUtil.isEmpty(mPrefs.getString(PREF_MAX_USER_MSG_ID))) {
			mPrefs.putString(PREF_MAX_USER_MSG_ID, "-1");
		}
		if (StringUtil.isEmpty(mPrefs.getString(PREF_SCREEN_WAKEUP_FLAG))) {
			mPrefs.putString(PREF_SCREEN_WAKEUP_FLAG, "Y");
		}
		if (StringUtil.isEmpty(PMSUtil.getServerUrl(context))) {
			PMSUtil.setServerUrl(context, API_SERVER_URL);
		}
		if (StringUtil.isEmpty(mPrefs.getString(PREF_PUSH_POPUP_ACTIVITY))) {
			mPrefs.putString(PREF_PUSH_POPUP_ACTIVITY, DEFAULT_PUSH_POPUP_ACTIVITY);
		}
		if (mPrefs.getInt(PREF_PUSH_POPUP_SHOWING_TIME) < 1) {
			mPrefs.putInt(PREF_PUSH_POPUP_SHOWING_TIME, 10000);
		}
	}

	/**
	 * getInstance
	 * 
	 * @param context
	 * @param projectId
	 * @return
	 */
	public static PMS getInstance (Context context, String projectId) {
		PMSUtil.setGCMProjectId(context, projectId);
		return getInstance(context);
	}

	/**
	 * getInstance
	 * 
	 * @param context
	 * @return
	 */
	public static PMS getInstance (Context context) {
		CLog.d("getInstance:projectId=" + PMSUtil.getGCMProjectId(context));
		if (instancePms == null) {
			instancePms = new PMS(context);
		}
		instancePms.setmContext(context);
		return instancePms;
	}

	public static PMSPopup getPopUpInstance () {
		return instancePmsPopup;
	}

//	/**
//	 * start mqtt service
//	 */
//	public void startMQTTService (Context context) {
//		if (FLAG_Y.equals(PMSUtil.getMQTTFlag(context))) {
//			context.sendBroadcast(new Intent(context, RestartReceiver.class).setAction(ACTION_START));
//		} else {
//			context.stopService(new Intent(context, MQTTService.class));
//		}
//	}
//
//	public void stopMQTTService (Context context) {
//		context.stopService(new Intent(context, MQTTService.class));
//	}

//	@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR1)
//	public void startCheckService (Context context) {
//		Intent i = new Intent(context, ServiceCheckReceiver.class);
//		i.setAction(ACTION_CHECK_SERVICE_START);
//		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR1) {
//			i.addFlags(Intent.FLAG_EXCLUDE_STOPPED_PACKAGES);
//		}
//		context.sendBroadcast(i);
//	}
//
//	public void stopCheckService (Context context) {
//		Intent i = new Intent(context, ServiceCheckReceiver.class);
//		i.setAction(ACTION_CHECK_SERVICE_CANCEL);
//		context.sendBroadcast(i);
//	}

	/**
	 * clear
	 * 
	 * @return
	 */
	public static boolean clear () {
		try {

			PMSUtil.setDeviceCertStatus(instancePms.mContext, DEVICECERT_PENDING);
			instancePms.unregisterReceiver();
			instancePms = null;
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	private void setmContext (Context context) {
		this.mContext = context;
	}

	private void unregisterReceiver () {
		Badge.getInstance(mContext).unregisterReceiver();
	}

	public void setPopupSetting (Boolean state, String title) {
		instancePmsPopup = PMSPopup.getInstance(mContext, mContext.getPackageName(), state, title);
	}

	/**
	 * set ServerUrl
	 * 
	 * @param serverUrl
	 */
	public void setServerUrl (String serverUrl) {
		CLog.i("setServerUrl");
		CLog.d("setServerUrl:serverUrl=" + serverUrl);
		PMSUtil.setServerUrl(mContext, serverUrl);
	}

	/**
	 * cust id 세팅
	 * 
	 * @param custId
	 */
	public void setCustId (String custId) {
		CLog.i("setCustId");
		CLog.d("setCustId:custId=" + custId);
		PMSUtil.setCustId(mContext, custId);
	}

	public void setPushToken(String token){
		PMSUtil.setGCMToken(mContext, token);
	}

	/**
	 * get cust id
	 * 
	 * @return
	 */
	public String getCustId () {
		CLog.i("getCustId");
		return PMSUtil.getCustId(mContext);
	}

	public void setMemberId (String memberId) {
		PMSUtil.setMemberId(mContext, memberId);
	}

	public String getMemberId () {
		return PMSUtil.getMemberId(mContext);
	}

	public void setIsPopupActivity (Boolean ispopup) {
		PMSUtil.setPopupActivity(mContext, ispopup);
	}

	public void setNotiOrPopup (Boolean isnotiorpopup) {
		PMSUtil.setNotiOrPopup(mContext, isnotiorpopup);
	}

	public void setCheckServiceInterval (int interval) {
		PMSUtil.setCheckServiceInterval(mContext, interval);
	}

	/**
	 * set inbox activity
	 * 
	 * @param inboxActivity
	 */
	public void setInboxActivity (String inboxActivity) {
		mPrefs.putString(PREF_INBOX_ACTIVITY, inboxActivity);
	}

	/**
	 * set push popup activity
	 * 
	 * @param classPath
	 */
	public void setPushPopupActivity (String classPath) {
		mPrefs.putString(PREF_PUSH_POPUP_ACTIVITY, classPath);
	}

	/**
	 * set pushPopup showing time
	 * 
	 * @param pushPopupShowingTime
	 */
	public void setPushPopupShowingTime (int pushPopupShowingTime) {
		mPrefs.putInt(PREF_PUSH_POPUP_SHOWING_TIME, pushPopupShowingTime);
	}

	/**
	 * get msg flag
	 * 
	 * @return
	 */
	public String getMsgFlag () {
		return mPrefs.getString(PREF_MSG_FLAG);
	}

	/**
	 * get noti flag
	 * 
	 * @return
	 */
	public String getNotiFlag () {
		return mPrefs.getString(PREF_NOTI_FLAG);
	}

	public String getDumpFlag () {
		return mPrefs.getString(PREF_DUMP_FLAG);
	}

	public void setNotiGroupFlag(boolean isNotiGroup)
	{
		mPrefs.putString(IPMSConsts.PREF_NOTI_GROUP_FLAG, isNotiGroup?"Y":"N");
	}
	public String getNotiGroupFlag()
	{
		return mPrefs.getString(IPMSConsts.PREF_NOTI_BACK_COLOR);
	}
    public void setNotiBackColor(String colorHexString)
    {
        mPrefs.putString(IPMSConsts.PREF_NOTI_BACK_COLOR, colorHexString);
    }
    public String getNotiBackColor()
    {
        return mPrefs.getString(IPMSConsts.PREF_NOTI_BACK_COLOR);
    }

	/**
	 * set large noti icon
	 * 
	 * @param resId
	 */
	public void setLargeNotiIcon (int resId) {
		mPrefs.putInt(PREF_LARGE_NOTI_ICON, resId);
	}

	/**
	 * set noti cound
	 * 
	 * @param resId
	 */
	public void setNotiSound (int resId) {
		mPrefs.putInt(PREF_NOTI_SOUND, resId);
	}

	/**
	 * set noti receiver
	 * 
	 * @param intentAction
	 */
	public void setNotiReceiver (String intentAction) {
		mPrefs.putString(PREF_NOTI_RECEIVER, intentAction);
	}

	/**
	 * set ring mode
	 * 
	 * @param isRingMode
	 */
	public void setRingMode (boolean isRingMode) {
		mPrefs.putString(PREF_RING_FLAG, isRingMode ? "Y" : "N");
	}

	/**
	 * set vibe mode
	 * 
	 * @param isVibeMode
	 */
	public void setVibeMode (boolean isVibeMode) {
		mPrefs.putString(PREF_VIBE_FLAG, isVibeMode ? "Y" : "N");
	}

	/**
	 * set popup noti
	 * 
	 * @param isShowPopup
	 */
	public void setPopupNoti (boolean isShowPopup) {
		mPrefs.putString(PREF_ALERT_FLAG, isShowPopup ? "Y" : "N");
	}

	/**
	 * set screen wakeup
	 * 
	 * @param isScreenWakeup
	 */
	public void setScreenWakeup (boolean isScreenWakeup) {
		mPrefs.putString(PREF_SCREEN_WAKEUP_FLAG, isScreenWakeup ? "Y" : "N");
	}

	public OnReceivePushListener getOnReceivePushListener () {
		return onReceivePushListener;
	}

	public void setOnReceivePushListener (OnReceivePushListener onReceivePushListener) {
		this.onReceivePushListener = onReceivePushListener;
	}

	/**
	 * set debugTag
	 * 
	 * @param tagName
	 */
	public void setDebugTAG (String tagName) {
		CLog.setTagName(tagName);
	}

	/**
	 * set debug mode
	 * 
	 * @param debugMode
	 */
	public void setDebugMode (boolean debugMode) {
		CLog.setDebugMode(mContext, debugMode);
	}

	/**
	 * set noti receiver class (Class name)
	 * @param intentAction
	 */
	public void setNotiReceiverClass (String intentAction) {
		mPrefs.putString(PREF_NOTI_RECEIVER_CLASS, intentAction);
	}

	/**
	 * set noti receiver (Intent action)
	 * @param intentAction
	 */
	public void setPushReceiverClass (String intentAction) {
		mPrefs.putString(PREF_PUSH_RECEIVER_CLASS, intentAction);
	}

	/*
	 * ===================================================== [start] database =====================================================
	 */
	/**
	 * select MsgGrp list
	 * 
	 * @return
	 */
	public Cursor selectMsgGrpList () {
		return mDB.selectMsgGrpList();
	}

	/**
	 * select MsgGrp
	 * 
	 * @param msgCode
	 * @return
	 */
	public MsgGrp selectMsGrp (String msgCode) {
		return mDB.selectMsgGrp(msgCode);
	}

	/**
	 * select new msg Cnt
	 * 
	 * @return
	 */
	public int selectNewMsgCnt () {
		return mDB.selectNewMsgCnt();
	}

	/**
	 * select Msg List
	 * 
	 * @param page
	 * @param row
	 * @return
	 */
	public Cursor selectMsgList (int page, int row) {
		return mDB.selectMsgList(page, row);
	}

	/**
	 * select Msg List
	 * 
	 * @param msgCode
	 * @return
	 */
	public Cursor selectMsgList (String msgCode) {
		return mDB.selectMsgList(msgCode);
	}

	/**
	 * select Msg
	 * 
	 * @param msgId
	 * @return
	 */
	public Msg selectMsgWhereMsgId (String msgId) {
		return mDB.selectMsgWhereMsgId(msgId);
	}

	/**
	 * select Msg
	 * 
	 * @param userMsgID
	 * @return
	 */
	public Msg selectMsgWhereUserMsgId (String userMsgID) {
		return mDB.selectMsgWhereUserMsgId(userMsgID);
	}

	/**
	 * select query
	 * 
	 * @param sql
	 * @return
	 */
	public Cursor selectQuery (String sql) {
		return mDB.selectQuery(sql);
	}

	/**
	 * update MsgGrp
	 * 
	 * @param msgCode
	 * @param values
	 * @return
	 */
	public long updateMsgGrp (String msgCode, ContentValues values) {
		return mDB.updateMsgGrp(msgCode, values);
	}

	/**
	 * update read msg
	 * 
	 * @param msgGrpCd
	 * @param firstUserMsgId
	 * @param lastUserMsgId
	 * @return
	 */
	public long updateReadMsg (String msgGrpCd, String firstUserMsgId, String lastUserMsgId) {
		return mDB.updateReadMsg(msgGrpCd, firstUserMsgId, lastUserMsgId);
	}

	/**
	 * update read msg where userMsgId
	 * 
	 * @param userMsgId
	 * @return
	 */
	public long updateReadMsgWhereUserMsgId (String userMsgId) {
		return mDB.updateReadMsgWhereUserMsgId(userMsgId);
	}

	/**
	 * update read msg where msgId
	 * 
	 * @param msgId
	 * @return
	 */
	public long updateReadMsgWhereMsgId (String msgId) {
		return mDB.updateReadMsgWhereMsgId(msgId);
	}

	/**
	 * delete Msg
	 * 
	 * @param userMsgId
	 * @return
	 */
	public long deleteUserMsgId (String userMsgId) {
		return mDB.deleteUserMsgId(userMsgId);
	}

	/**
	 * delete Msg
	 * 
	 * @param MsgId
	 * @return
	 */
	public long deleteMsgId (String MsgId) {
		return mDB.deleteMsgId(MsgId);
	}

	/**
	 * delete MsgGrp
	 * 
	 * @param msgCode
	 * @return
	 */
	public long deleteMsgGrp (String msgCode) {
		return mDB.deleteMsgGrp(msgCode);
	}

	/**
	 * delete expire Msg
	 * 
	 * @return
	 */
	public long deleteExpireMsg () {
		return mDB.deleteExpireMsg();
	}

	/**
	 * delete empty MsgGrp
	 * 
	 * @return
	 */
	public void deleteEmptyMsgGrp () {
		mDB.deleteEmptyMsgGrp();
	}

	/**
	 * delete all
	 */
	public void deleteAll () {
		mDB.deleteAll();
	}

	/*
	 * ===================================================== [end] database =====================================================
	 */

	private static Stack<Intent> serviceStack = new Stack<>();

	public static Stack<Intent> getServiceStack()
	{
		return serviceStack;
	}

	public static void setServiceStack(Intent serviceId)
	{
		serviceStack.push(serviceId);
	}
}
