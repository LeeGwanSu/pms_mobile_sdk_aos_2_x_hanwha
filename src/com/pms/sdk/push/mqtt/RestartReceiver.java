package com.pms.sdk.push.mqtt;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.text.TextUtils;
import android.util.Log;

import com.pms.sdk.IPMSConsts;
import com.pms.sdk.common.util.CLog;
import com.pms.sdk.common.util.PMSUtil;
import com.pms.sdk.common.util.PhoneState;
import com.pms.sdk.common.util.StringUtil;

import java.net.URI;

import static android.content.Context.MODE_PRIVATE;

/**
 * Private PUSH Restart Receiver
 * @author haewon
 *
 */
public class RestartReceiver extends BroadcastReceiver implements IPMSConsts
{
	public static final long DEFAULT_SCHEDULE_TIME = 1000 * 60 * 1;

	@Override
	public synchronized void onReceive (final Context context, Intent intent)
	{
		if(intent != null)
		{
			String action = intent.getAction();
//			MQTTScheduler scheduler = MQTTScheduler.getInstance();

			if(!TextUtils.isEmpty(action))
			{
				int device = Build.VERSION.SDK_INT;
				Log.d("TAG", "Device build version: " + device + ", Release version: " + Build.VERSION.RELEASE);

				if(ACTION_FORCE_START.equals(action))
				{
					try
					{
						String url = PMSUtil.getMQTTServerUrl(context, getConnectServerNum(context));
						if (TextUtils.isEmpty(url))
						{
							CLog.w("url is empty");
							return;
						}

						URI serverUri = new URI(url);

						String serverProtocol = serverUri.getScheme();
						String serverHost = serverUri.getHost();
						int serverPort = serverUri.getPort();

						String clientId = PMSUtil.getAppUserId(context);
						if(!TextUtils.isEmpty(clientId))
						{
							int keepAlive = (int) DEFAULT_SCHEDULE_TIME / 1000;
							MQTTBinder.newInstance(context).withInfo(clientId, serverProtocol, serverHost, serverPort, keepAlive);
							MQTTBinder.ConnectInfo service = MQTTBinder.getInstance().getChild();
							if (service != null)
							{
								service.start(new MQTTBinder.IMQTTServiceCallback()
								{
									// connection complete
									@Override
									public void onConnect(MQTTBinder.ConnectInfo child)
									{
										child.closeToAfterMillisecond(DEFAULT_SCHEDULE_TIME);
									}

									// connection close
									@Override
									public void onFinish()
									{
										CLog.d("[ Binder finish ]");
									}
								});
							}
						}
					}
					catch (Exception e)
					{
						CLog.e("Receiver error(mqtt) " + e.getMessage());
					}
				}
				else
				{
					CLog.w("[ RestartReceiver ] not support action: " + action);
				}
			}
			else
			{
				CLog.w("[ RestartReceiver ] intent action is empty");
			}
		}
		else
		{
			CLog.w("[ RestartReceiver ] intent is null");
		}
	}
	private int getConnectServerNum (Context context) {
		int ram = 0;
		String temp = "";
		SharedPreferences m_Prefs = context.getSharedPreferences(PREF_FILE_NAME, MODE_PRIVATE);
		String PREF_MQTT_SERVER_NUMBER = "pref_mqtt_server_number";
		try {
			if (m_Prefs.getInt(PREF_MQTT_SERVER_NUMBER, -1) == -1) {
				temp = PhoneState.getSimSerialNumber(context);
				int num = Integer.valueOf(temp.substring((temp.length() - 1), temp.length()));
				if (StringUtil.isEmpty(temp)) {
					ram = randomRange(1, 2);
				}

				if ((num % 2) == 0) {
					ram = 2;
				} else if ((num % 2) != 0) {
					ram = 1;
				}
				m_Prefs.edit().putInt(PREF_MQTT_SERVER_NUMBER, ram).commit();
				return ram;
			} else {
				return m_Prefs.getInt(PREF_MQTT_SERVER_NUMBER, -1);
			}
		} catch (Exception e) {
			CLog.e(e.getMessage());
			ram = randomRange(1, 2);
			m_Prefs.edit().putInt(PREF_MQTT_SERVER_NUMBER, ram).commit();
			return ram;
		}
	}
	private int randomRange (int n1, int n2) {
		return (int) (Math.random() * (n2 - n1 + 1)) + n1;
	}
}