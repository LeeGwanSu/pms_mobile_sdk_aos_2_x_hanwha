package com.hanwha.app.push;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;
import com.hanwha.app.activity.MainActivity;
import com.pms.sdk.bean.PushMsg;
import com.pms.sdk.common.util.CLog;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Notibar Click 시 실행되는 class
 */
public class PushNotiReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        CLog.i("onReceive");
        Intent i = new Intent(context, MainActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        i.putExtras(intent.getExtras());

        // 버튼 링크값 가져오는 방법.
        try {
            PushMsg pushMsg = new PushMsg(i.getExtras());
            JSONObject btnLink = new JSONObject(pushMsg.data);
            Toast.makeText(context, btnLink.get("l").toString(), Toast.LENGTH_SHORT).show();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        context.startActivity(i);
    }
}
