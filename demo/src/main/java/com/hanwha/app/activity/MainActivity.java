package com.hanwha.app.activity;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.pmsdemo.R;
import com.pms.sdk.IPMSConsts;
import com.pms.sdk.PMS;
import com.pms.sdk.PMSPopup;
import com.pms.sdk.api.APIManager;
import com.pms.sdk.api.request.*;
import com.pms.sdk.bean.Msg;
import com.pms.sdk.common.util.PMSPopupUtil;
import com.pms.sdk.common.util.PMSUtil;
import com.pms.sdk.common.util.Prefs;
import com.pms.sdk.db.PMSDB;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;


public class MainActivity extends Activity implements IPMSConsts, Serializable {

    private static final long serialVersionUID = 1L;

    private transient PMS pms = null;
    private PMSPopup pmsPopup = null;
    /**
     * 팝업 창에 대한 버튼 이벤트 리스너
     */
    private final PMSPopupUtil.btnEventListener btnEvent = new PMSPopupUtil.btnEventListener() {

        private static final long serialVersionUID = 1L;

        @Override
        public void response() {
            pmsPopup.getActivity().finish();
        }
    };
    private final PMSPopupUtil.btnEventListener btnEvent1 = new PMSPopupUtil.btnEventListener() {

        private static final long serialVersionUID = 1L;

        @Override
        public void response() {
            pmsPopup.startNotiReceiver();
        }
    };
    private Context mContext = null;
    private EditText mEdtCustId = null;
    private Button mBtnDeviceCert = null;
    private Button mBtnLoginPms = null;
    private Button mBtnNewMsg = null;
    private Button mBtnReadMsg = null;
    private Button mBtnSetConfig = null;
    private Button mBtnLogoutPms = null;
    private TextView mTxtResult = null;
    private transient OnClickListener onClickListener = new OnClickListener() {

        @Override
        public void onClick(View v) {
            int id = v.getId();
            mTxtResult.setText("loading...");

            switch (id) {
                case R.id.btn_device_cert:
                    pms.setCustId(mEdtCustId.getText().toString()); // Cust ID 입력
                    pms.setMemberId("05822830"); // Member ID 입력

                    /**
                     * deviceCert (앱이 실행 되는 시점에서 호출 해주시면 됩니다.) 첫번째 파라미터는 CRM데이터 연동을 위한 파라미터로 null로 념겨주시면 됩니다.
                     */
                    new DeviceCert(mContext).request(null, new APIManager.APICallback() {
                        @Override
                        public void response(String arg0, JSONObject arg1) {
                            mTxtResult.setText(arg1.toString());
                        }
                    });
                    break;

                case R.id.btn_login_pms:
                    new changeDevice(mContext).request("Y", new APIManager.APICallback() {
                        @Override
                        public void response(String code, JSONObject json) {
                            mTxtResult.setText(json.toString());
                        }
                    });
                    break;

                case R.id.btn_new_msg:
                    // 현재 가지고 있는 메시지의 max user msg id를 가져올 수 있습니다.
                    String req = new Prefs(mContext).getString(PMS.PREF_MAX_USER_MSG_ID);

                    /**
                     * 서버에서 메시지를 가져 와서 SQLite에 저장하는 request입니다. callback에서 pms.selectMsgList(1, 9999)과 같이 cursor를 가져와 쓰시거나, newMsg완료시점에 broadcasting을
                     * 하기 때문에, PMS.RECEIVER_REQUERY로 receiver를 받으셔서 메시지를 가져오셔도 됩니다. 파라미터 관련해서는 api문서를 참조하시면 되겠니다.
                     */
                    new NewMsg(mContext).request(PMS.TYPE_NEXT, req, "-1", "1", "30", new APIManager.APICallback() {
                        @Override
                        public void response(String arg0, JSONObject arg1) {
                            // 메시지를 cursor형태로 가져올수 있습니다.
                            Cursor c = pms.selectMsgList(1, 9999);
                            mTxtResult.setText("msgListSize:" + c.getCount() + "\n\n" + arg1.toString());
                            PMSDB pmsDB = PMSDB.getInstance(mContext);
                            pmsDB.showAllTable(Msg.TABLE_NAME);
                        }
                    });
                    break;

                case R.id.btn_read_msg:
                    /**
                     * 메세지 읽을 처리 Class 입니다.
                     */
                    JSONArray jsAr = new JSONArray();
                    jsAr.put(PMSUtil.getReadParam("1")); // MsgId 삽입.

                    new ReadMsg(mContext).request(jsAr, new APIManager.APICallback() {
                        @Override
                        public void response(String arg0, JSONObject arg1) {
                            mTxtResult.setText(arg1.toString());
                        }
                    });
                    break;

                case R.id.btn_set_config:
                    /**
                     * msg & noti flag 값을 저장하는 Class 입니다.
                     */
                    new SetConfig(mContext).request("Y", "Y", new APIManager.APICallback() {
                        @Override
                        public void response(String arg0, JSONObject arg1) {
                            mTxtResult.setText(arg1.toString());
                        }
                    });
                    break;

                case R.id.btn_logout_pms:
                    /**
                     * Logout 을 실행하는 Class 입니다.
                     */
                    new LogoutPms(mContext).request(new APIManager.APICallback() {
                        @Override
                        public void response(String arg0, JSONObject arg1) {
                            mTxtResult.setText(arg1.toString());
                        }
                    });
                    break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mContext = this;

        setPMSSetting();
        // setPMSPopupSetting(); Default Popup 사용 안할시.

        mEdtCustId = (EditText) findViewById(R.id.edt_cust_id);

        mBtnDeviceCert = (Button) findViewById(R.id.btn_device_cert);
        mBtnLoginPms = (Button) findViewById(R.id.btn_login_pms);
        mBtnNewMsg = (Button) findViewById(R.id.btn_new_msg);
        mBtnReadMsg = (Button) findViewById(R.id.btn_read_msg);
        mBtnSetConfig = (Button) findViewById(R.id.btn_set_config);
        mBtnLogoutPms = (Button) findViewById(R.id.btn_logout_pms);

        mTxtResult = (TextView) findViewById(R.id.txt_result);

        mBtnDeviceCert.setOnClickListener(onClickListener);
        mBtnLoginPms.setOnClickListener(onClickListener);
        mBtnNewMsg.setOnClickListener(onClickListener);
        mBtnReadMsg.setOnClickListener(onClickListener);
        mBtnSetConfig.setOnClickListener(onClickListener);
        mBtnLogoutPms.setOnClickListener(onClickListener);

        mEdtCustId.setText(pms.getCustId());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        PMS.clear();
    }

    private void setPMSSetting() {
        // pms 기본 셋팅입니다.
        pms = PMS.getInstance(mContext, "997255892867");
        pms.setPopupSetting(true, "Hanwha");
        pms.setNotiReceiver("com.pms.hanwha.push.notification");
        pms.setPushPopupActivity("com.hanwha.app.activity.PushPopupActivity");
        pms.setDebugTAG("Hanwha");
        pms.setDebugMode(true);
        pms.setRingMode(true);
        pms.setVibeMode(true);
        pms.setScreenWakeup(true);
        pms.setPopupNoti(true);
        pms.setIsPopupActivity(true);
        pms.setCheckServiceInterval(60000);
    }

    private void setPMSPopupSetting() {
        // pms popup 기본 셋팅입니다.
        pmsPopup = PMS.getPopUpInstance();
        pmsPopup.setXmlAndDefaultFlag(true);

        setImage();
        // setXML();

        pmsPopup.commit();
    }

    private void setXML() {
        // xml 파일로 팝업창을 생성시
        pmsPopup.setLayoutXMLTextResId("pms_text_popup");
        pmsPopup.setXMLTextButtonType("TextView", "TextView");
        pmsPopup.setXMLTextButtonTagName("button1", "button2");

        pmsPopup.setLayoutXMLRichResId("pms_rich_popup");
        pmsPopup.setXMLRichButtonType("TextView");
        pmsPopup.setXMLRichButtonTagName("button1");

        pmsPopup.setTextBottomBtnClickListener(btnEvent, btnEvent1);
        pmsPopup.setRichBottomBtnClickListener(btnEvent);
    }

    private void setImage() {
        // 이미지 파일로 적용시 셋팅 예시

        pmsPopup.setPopUpBackColor(128, 128, 128, 200);
        pmsPopup.setPopupBackImgResource("pms_bg_popup");

        // Top Layout Setting
        pmsPopup.setTopLayoutFlag(true);
        pmsPopup.setTopBackColor(56, 98, 196, 255);
        pmsPopup.setTopTitleType("image");
        pmsPopup.setTopTitleTextColor(255, 255, 255);
        pmsPopup.setTopTitleTextSize(25);
        pmsPopup.setTopTitleName("Hanwha");
        pmsPopup.setTopTitleImgResource("pms_img_logo");

        // Content Layout Setting
        pmsPopup.setContentBackImgResource("myfav_titleback");
        pmsPopup.setContentBackColor(255, 255, 255, 255);
        pmsPopup.setContentTextColor(0, 0, 0);
        pmsPopup.setContentTextSize(20);

        // Bottom Button Layout Setting
        pmsPopup.setBottomTextBtnCount(2);
        pmsPopup.setBottomRichBtnCount(1);
        pmsPopup.setBottomBackColor(224, 224, 224, 255);
        pmsPopup.setBottomBackImgResource("myfav_titleback");
        pmsPopup.setBottomTextViewFlag(true);
        pmsPopup.setBottomTextBtnCount(2);
        pmsPopup.setBottomRichBtnCount(1);
        pmsPopup.setBottomBtnTextName("닫  기", "자세히 보기");
        pmsPopup.setBottomBtnTextColor(255, 255, 255);
        pmsPopup.setBottomBtnTextSize(10);
        pmsPopup.setBottomBtnImageResource("mypage_stamp_confirmbtn", "mypage_stamp_cancelbtn");
        pmsPopup.setBottomBtnImageResource("pms_btn_text_close_off", "pms_btn_text_detail_off");
        pmsPopup.setTextBottomBtnClickListener(btnEvent, btnEvent1);
        pmsPopup.setRichBottomBtnClickListener(btnEvent);
    }
}
